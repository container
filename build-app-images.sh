#!/bin/bash

BASE_D="${BASH_SOURCE%/*}"

function run_build {
  local img_name="$1"
  local img_name_helsinki="docker.helsinki.at/$img_name"
  local build_dir="$2"

  echo -e "\033[1m****************\033[0m"
  echo -e "  \033[1mbuilding: $img_name\033[0m"
  echo ""

  "$BASE_D/_build.sh" "$img_name_helsinki" "$BASE_D/$build_dir"
  result=$?

  echo ""
  if [ "$result" -ne 0 ]; then
    echo -e " \033[1;31m*** FAILED ***\033[0m";
  else
    echo -e " \033[1;32m*** Success ***\033[0m";
  fi
  echo ""
}

run_build "ffmpeg:bullseye" "ffmpeg/bullseye"
run_build "gstreamer:bullseye" "gstreamer/bullseye"
run_build "icecast:bullseye" "icecast/bullseye"
