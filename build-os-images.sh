#!/bin/bash

BASE_D="${BASH_SOURCE%/*}"

function run_build {
  local img_name="$1"
  local img_name_helsinki="docker.helsinki.at/$img_name"
  local build_dir="$2"

  echo -e "\033[1m****************\033[0m"
  echo -e "  \033[1mbuilding: $img_name\033[0m"
  echo ""

  if [ "$UID" -ne 0 ]; then
    sudo docker pull "$img_name"
  else
    docker pull "$img_name"
  fi
  result=$?

  if [ $result -eq 0 ]; then
    echo ""
    "$BASE_D/_build.sh" "$img_name_helsinki" "$BASE_D/$build_dir"
    result=$?
  fi

  echo ""
  if [ "$result" -ne 0 ]; then
    echo -e " \033[1;31m*** FAILED ***\033[0m";
  else
    echo -e " \033[1;32m*** Success ***\033[0m";
  fi
  echo ""
}

## debian
for codename in buster bullseye; do
  run_build "debian:$codename" "debian/$codename"
done

# ## ubuntu
# for codename in focal jammy; do
#   run_build "ubuntu:$codename" "ubuntu/$codename"
# done
#
