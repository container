#!/bin/bash

img_name="$1"
shift
build_dir="$1"
shift

function print_usage {
  echo ""
  echo "Usage: $0 <image-name> <build-dir> [ .. <additional args to docker build> ]"
  echo ""
  echo "  for example:"
  echo "    $0 docker.helsinki.at/debian:stretch debian/stretch/ --no-cache"
  echo "  will run:"
  echo "    docker build -t docker.helsinki.at/debian:stretch --no-cache debian/stretch/"
  echo ""
}

if [ -z "$img_name" ] || [ -z "$build_dir" ]; then
  print_usage
  exit 1
fi

if [ ! -d "$build_dir" ]; then
  echo "Error: '$build_dir' is not a directory"
  exit 1
fi

set -e

BASE_D="${BASH_SOURCE%/*}"
STAGING_D=$(mktemp -d -t rh-docker-build.XXXX)

function cleanup {
  rm -rf "$STAGING_D"
}
trap cleanup EXIT

cp -r "$build_dir" "$STAGING_D"
BUILD_D="$STAGING_D"/$(basename "$build_dir")
cp -r "$BASE_D/common" "$BUILD_D/"


if [ "$UID" -ne 0 ]; then
  exec sudo docker build -t "$img_name" "$@" "$BUILD_D"
fi
exec docker build -t "$img_name" "$@" "$BUILD_D"
