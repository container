#!/bin/bash

REPO_BASE_D="/srv/docker-registry/docker/registry/v2/repositories"

cd "$REPO_BASE_D"
declare -A revisions
for repo in $(ls -d *); do
  revisions=()
  echo "$repo: "
  echo " revisions:"
  pushd "$repo/_manifests/revisions/" > /dev/null
  for algo in $(ls -d *); do
    pushd "$algo" > /dev/null
    for revision in $(ls -d *); do
      echo "  $algo:$revision"
      revisions["$algo:$revision"]=0
    done
    popd > /dev/null
  done
  popd > /dev/null
  echo " tags current:"
  pushd "$repo/_manifests/tags/" > /dev/null
  for tag in $(ls -d *); do
     link=$(cat "$tag/current/link")
     echo "  $tag: $link"
     revisions["$link"]=1
  done
  popd > /dev/null
  echo ""

  for rev in "${!revisions[@]}"; do
    if [ "${revisions[$rev]}" -eq 1 ]; then
       echo -e " keeping \033[1;32m""$rev""\033[0m";
    else
       echo -n -e " removing \033[1;31m""$rev""\033[0m";
       rm -rf "$repo/_manifests/revisions/$(echo $rev | tr ':' '/')"
       rm -rf "$repo/_manifests/tags/"*"/index/$(echo $rev | tr ':' '/')"
       echo " done."
    fi
  done

  echo ""
done

docker exec -it docker-registry registry garbage-collect /etc/docker/registry/config.yml
